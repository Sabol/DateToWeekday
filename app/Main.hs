module Main where

import Lib
import DateToWeekday
import Data.List.Split

main :: IO ()
main = do
  putStrLn "Enter a date of form mm/dd/yyyy"
  date <- getLine 
  -- parse out some stuff from the date string
  -- this looks ugly...
  let date'     = splitOn "/" date
      m         = read (date' !! 0) :: Int 
      d         = read (date' !! 1) :: Int
      y         = read (date' !! 2) :: Int
      weekDay   = dateToWeekday m d y
  putStrLn $ date ++ " is a " ++ weekDay
