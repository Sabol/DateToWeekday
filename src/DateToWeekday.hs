module DateToWeekday 
    ( dateToWeekday  
    ) where

{-
 - How do we do this shite? So I guess we can start by calculating the 
 - set the start date to the date in which the Holy Roman Empire (Catholic states) set the calendar
 - start.
 - 01/01/1544
 -}

-- The ordering of this has to be weird
-- due to the result of modding the number of days
-- the date has.
-- This is because if we mod a number of days that is
-- evenly divisble by 7 we get 0 which would not be the
-- correct index of the day, so just shift the list of
-- days to fit with this skewing as seen below.
daysOfWeek :: [String]
daysOfWeek = [ "Friday"
             , "Saturday"
             , "Sunday"
             , "Monday"
             , "Tuesday"
             , "Wednesday"
             , "Thursday"
             ]

-- Calculate the day of the week in which a date would fall, algorithmically.
-- Let's get the number of years that have passed?
-- Really we don't need to, we just need to calculate how many days
-- the year given is.
-- common years - 365 days
-- leap years - 366 days
-- count up the number of days that the year is including 01/01/yyyy
-- Then add to this the days that we would have gained due to leap years
-- then add the number of days we would have gained due to how many months
-- and days we are into the given year. This give us the total days 
-- since the start of the calendar.
-- Lets check to see if the start date and if the end date are leap years

yearsPassed :: Int -> Int -> Int
yearsPassed a b = b - a

totalDaysPassed :: Int -> Int
totalDaysPassed y = 
  let leapYears = numLeapYears y
      commonYears = y - leapYears 
      -- Add one to include the start day
  in commonYears * 365 + leapYears * 366

-- What is the quickest way that we can find how many leap years there are
-- between two dates?
-- I suppose the easiest way would be to just count them...
-- Count them from 0 to year
numLeapYears :: Int -> Int
numLeapYears y = 
  numLeapYears' [0..y] 0 

numLeapYears' :: [Int] -> Int -> Int
numLeapYears' [] count = count
numLeapYears' (x:xs) count =
  if (isLeapYear x) == True then numLeapYears' xs (count+1)
  else numLeapYears' xs count

isLeapYear :: Int -> Bool
isLeapYear y = 
  if y `mod` 4 == 0 then
    if y `mod` 100 == 0 then
      if y `mod` 400 == 0 then
        True
      else False
    else True
  else False

-- How far are we into a year from 01/01
-- Takes a month (MM), day (DD), year (YYYY)
daysIntoYear :: Int -> Int -> Int -> Int
daysIntoYear m d y = 
  let daysInMonths  = [31,28,31,30,31,30,31,31,30,31,30,31]
      daysInMonths' = [31,29,31,30,31,30,31,31,30,31,30,31]
  in if (isLeapYear y) == True then 
       sum (take (m-1) daysInMonths') + d
     else
       sum (take (m-1) daysInMonths) + d

          
-- d - day, m - month, y - last two digits of the year.
dateToWeekday :: Int -> Int -> Int -> String 
dateToWeekday m d y =  
  let numDays = (totalDaysPassed (y - 1)) + 1 + (daysIntoYear m d y) 
  in  daysOfWeek !! (numDays `mod` 7)

